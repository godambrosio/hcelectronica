class AddPatientRefToDiagnostics < ActiveRecord::Migration
  def change
    add_reference :diagnostics, :patient, index: true, foreign_key: true
  end
end
