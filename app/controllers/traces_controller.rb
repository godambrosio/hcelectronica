class TracesController < ApplicationController
  before_action :set_trace, only: [:show, :edit, :update, :destroy]

  # GET /traces
  # GET /traces.json
  def index
    @traces = Trace.all
  end

  # GET /traces/1
  # GET /traces/1.json
  def show
  end

  # GET /traces/new
  def new
    @inscription = Inscription.find(params[:inscription_id])
    @trace = Trace.new
    @patient = Patient.find(session[:patient_id])
    @user_institution = @inscription.user_institution
  end

  # GET /traces/1/edit
  def edit
    @inscription = @trace.inscription
    @patient = @inscription.patient
    @user_institution = @inscription.user_institution
  end

  # POST /traces
  # POST /traces.json
  def create
    @inscription = Inscription.find(params[:inscription_id])
    @user_institution = @inscription.user_institution
    @trace = Trace.new(trace_params)
    @patient = Patient.find(session[:patient_id])
    @trace.inscription = @inscription
    respond_to do |format|
      if @trace.save
        format.html { redirect_to user_institution_patient_path(@user_institution,@patient), notice: 'Trace was successfully created.' }
        format.json { render :show, status: :created, location: @trace }
      else
        format.html { render :new }
        format.json { render json: @trace.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /traces/1
  # PATCH/PUT /traces/1.json
  def update
    @inscription = @trace.inscription
    @patient = @inscription.patient
    @user_institution = @inscription.user_institution
    respond_to do |format|
      if @trace.update(trace_params)
        format.html { redirect_to user_institution_patient_path(@user_institution,@patient), notice: 'Trace was successfully updated.' }
        format.json { render :show, status: :ok, location: @trace }
      else
        format.html { render :edit }
        format.json { render json: @trace.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /traces/1
  # DELETE /traces/1.json
  def destroy
    @inscription = @trace.inscription
    @patient = @inscription.patient
    @user_institution = @inscription.user_institution
    @trace.destroy
    respond_to do |format|
      format.html { redirect_to user_institution_patient_path(@user_institution,@patient), notice: 'Trace was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trace
      @trace = Trace.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trace_params
      params.require(:trace).permit(:description)
    end
end
