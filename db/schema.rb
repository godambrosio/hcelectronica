# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160206150242) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "diagnostics", force: :cascade do |t|
    t.text     "description"
    t.text     "medicament"
    t.date     "visit"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "inscription_id"
  end

  add_index "diagnostics", ["inscription_id"], name: "index_diagnostics_on_inscription_id", using: :btree

  create_table "inscription_files", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "link"
    t.integer  "inscription_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "inscription_files", ["inscription_id"], name: "index_inscription_files_on_inscription_id", using: :btree

  create_table "inscriptions", force: :cascade do |t|
    t.integer  "user_institution_id"
    t.integer  "patient_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "inscriptions", ["patient_id"], name: "index_inscriptions_on_patient_id", using: :btree
  add_index "inscriptions", ["user_institution_id"], name: "index_inscriptions_on_user_institution_id", using: :btree

  create_table "institutions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "patients", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at",              default: '2015-10-15 00:00:00', null: false
    t.datetime "updated_at",              default: '2015-10-15 00:00:00'
    t.date     "birthdate"
    t.string   "identification_document"
    t.string   "marital_status"
    t.integer  "childrens"
    t.string   "addres"
    t.string   "personal_phone"
    t.string   "cell_phone"
    t.string   "occupation"
    t.string   "os_name"
    t.string   "os_identification"
    t.text     "past"
    t.text     "past_family"
    t.text     "toxic_abit"
    t.text     "others"
    t.string   "sex"
    t.string   "location"
    t.integer  "postal_code"
    t.string   "city"
    t.string   "email"
  end

  create_table "traces", force: :cascade do |t|
    t.text     "description"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "inscription_id"
  end

  add_index "traces", ["inscription_id"], name: "index_traces_on_inscription_id", using: :btree

  create_table "user_institutions", force: :cascade do |t|
    t.integer  "institution_id"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "user_institutions", ["institution_id"], name: "index_user_institutions_on_institution_id", using: :btree
  add_index "user_institutions", ["user_id"], name: "index_user_institutions_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "diagnostics", "inscriptions"
  add_foreign_key "inscription_files", "inscriptions"
  add_foreign_key "inscriptions", "patients"
  add_foreign_key "inscriptions", "user_institutions"
  add_foreign_key "traces", "inscriptions"
  add_foreign_key "user_institutions", "institutions"
  add_foreign_key "user_institutions", "users"
end
