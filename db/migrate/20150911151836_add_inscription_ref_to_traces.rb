class AddInscriptionRefToTraces < ActiveRecord::Migration
  def change
    add_reference :traces, :inscription, index: true, foreign_key: true
  end
end
