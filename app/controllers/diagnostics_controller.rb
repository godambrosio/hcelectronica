class DiagnosticsController < ApplicationController
  before_action :authenticate_user!
  def new
    @inscription = Inscription.find(params[:inscription_id])
    @patient = Patient.find(session[:patient_id])
    @user_institution = @inscription.user_institution
    @diagnostic = Diagnostic.new
  end
  def create
    @inscription = Inscription.find(params[:inscription_id])
    @diagnostic = Diagnostic.new(diagnostic_params)
    @diagnostic.inscription = @inscription
    @user_institution = @inscription.user_institution
    @patient = Patient.find(session[:patient_id])
    respond_to do |format|
      if @diagnostic.save
        format.html { redirect_to user_institution_patient_path(@user_institution,@patient), notice: 'Diagnostic was successfully created.' }
        format.json { render :show, status: :created, location: @diagnostic }
      else
        format.html { render :new }
        format.json { render json: @diagnostic.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @diagnostic = Diagnostic.find(params[:id])
    @inscription = @diagnostic.inscription
    @patient = @inscription.patient
    @user_institution = @inscription.user_institution
  end

  def update
    @diagnostic = Diagnostic.find(params[:id])
    @patient = @diagnostic.inscription.patient
    @user_institution = @diagnostic.inscription.user_institution
    respond_to do |format|
      if @diagnostic.update(diagnostic_params) then
        format.html {redirect_to user_institution_patient_path(@user_institution,@patient), notice: 'Diagnostic was successfully updated'}
      end
    end
  end

  def destroy
    @diagnostic = Diagnostic.find(params[:id])
    @patient = @diagnostic.inscription.patient
    @user_institution = @diagnostic.inscription.user_institution
    respond_to do |format|
      if @diagnostic.destroy then
        format.html {redirect_to user_institution_patient_path(@user_institution, @patient), notice: 'Diagnostic deleted'}
      end
    end
  end

  private

  def diagnostic_params
    params.require(:diagnostic).permit(:description, :medicament, :visit)
  end
end
