class ChangeDescriptionTypeInDiagnostics < ActiveRecord::Migration
  def change
    change_column :diagnostics, :description, :text
    change_column :diagnostics, :medicament, :text
  end
end
