class UserInstitutionsController <ApplicationController
    before_action :authenticate_user!
  def index
    @user = current_user
    @list = UserInstitution.includes(:institution).where("user_institutions.user_id = ?", current_user.id).order("institutions.name ASC")
    @one_can_delete = one_can_delete(@list)
  end

  def new
    @user = current_user
    @list = UserInstitution.includes(:institution).where("user_institutions.user_id = ?", current_user.id)
    @user_institution = UserInstitution.new
    @institutions = Institution.all
  end

  def destroy
    @user_institution = UserInstitution.find(params[:id])
    if !@user_institution.patients.any? then
      @user_institution.destroy
      message = "Institution was succesfully destroyed"
    else
      message = "Can't destroy because that Institution have patients"
    end
    respond_to do |format|
      format.html {redirect_to user_institutions_path, notice: message}
    end
  end

  def create
    @user = current_user
    if (@user.institution_ids.any?) then
      params[:user][:institution_ids].each do |item |
        @user.institution_ids = @user.institution_ids << item
      end
    else
      @user.institution_ids =  params[:user][:institution_ids]
    end
    if @user.save then
      respond_to do |format|
        format.html {redirect_to user_institutions_path, notice: "Succesfully asociated"}
      end
    end
  end

  private

  def one_can_delete(list)
    list.each do |item|
      if (item.can_delete) then
        logger.debug "Entro con item: " + item.id.to_s
        return true
      end
    end
    return false
  end

end
