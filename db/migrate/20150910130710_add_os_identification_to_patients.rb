class AddOsIdentificationToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :os_identification, :string
  end
end
