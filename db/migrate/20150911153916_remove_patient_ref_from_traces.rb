class RemovePatientRefFromTraces < ActiveRecord::Migration
  def change
    remove_reference :traces, :patient, index: true, foreign_key: true
  end
end
