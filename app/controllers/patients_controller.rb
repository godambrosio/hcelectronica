class PatientsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /patients
  # GET /patients.json
  def index
    @user = current_user
    if (params.include?(:user_institution_id))
      @user_institution = UserInstitution.find(params[:user_institution_id])
      @patients = @user_institution.patients.order("last_name ASC")
    else
      @patients = Patient.all.order("last_name ASC")
      render :index_all
    end
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    session[:patient_id] = @patient.id
    if (params.include?(:user_institution_id))
      @user_institution = UserInstitution.find(params[:user_institution_id])
      @inscription = @patient.inscriptions.find_by_user_institution_id(@user_institution.id)
      @diagnostics = @patient.inscriptions.find_by_user_institution_id(@user_institution.id).diagnostics
      @traces = @patient.inscriptions.find_by_user_institution_id(@user_institution.id).traces
      @files = @patient.inscriptions.find_by_user_institution_id(@user_institution.id).inscription_files
    else
      @patient = Patient.find(params[:id])
      render :showAll
    end
  end

  # GET /patients/new
  def new
    @patients = Patient.all
    @patient = Patient.new
    @user_institution = UserInstitution.find(params[:user_institution_id])
  end

  # GET /patients/1/edit
  def edit
    if (params.include?(:user_institution_id))
      @user_institution = UserInstitution.find(params[:user_institution_id])
    else
      render :editAll
    end
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(patient_params)
    @user_institution = UserInstitution.find(params[:user_institution_id])
    if @patient.valid? then
      @user_institution.patients << @patient
    end
    respond_to do |format|
      if @patient.save && @user_institution.save then
        format.html { redirect_to user_institution_patients_path(@user_institution), notice: 'Patient was successfully created.' }
        format.json { render :show, status: :created, location: @patient }
      else
        format.html { render :new }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    if params.include?(:user_institution_id) then
      @user_institution = UserInstitution.find(params[:user_institution_id])
      respond_to do |format|
        if @patient.update(patient_params)
          format.html { redirect_to user_institution_patient_path(@user_institution,@patient), notice: 'Patient was successfully updated.' }
          format.json { render :show, status: :ok, location: @patient }
        else
          format.html { render :edit }
          format.json { render json: @patient.errors, status: :unprocessable_entity }
        end
      end
    else
      respond_to do |format|
        if @patient.update(patient_params)
          format.html { redirect_to patient_path(@patient), notice: 'Patient was successfully updated.'}
          format.json { render :show, status: :ok, location: @patient }
        else
          format.html { render :edit }
          format.json { render json: @patient.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_params
      params.require(:patient).permit(:first_name, :last_name, :birthdate, :identification_document, :marital_status, :addres, :personal_phone, :cell_phone, :occupation, :os_name, :os_identification, :past, :past_family, :toxic_abit, :others, :childrens)
    end

    def diagnostic_params
      params.require(:patient).permit(diagnostics_attributes: [:visit, :description, :medicament])
    end
end
