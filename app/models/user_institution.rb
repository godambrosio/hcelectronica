class UserInstitution < ActiveRecord::Base
  belongs_to :institution
  belongs_to :user

  has_many :inscriptions
  has_many :patients, through: :inscriptions

  def can_delete
    if !patients.any? then
      true
    else
      false
    end
  end

end
