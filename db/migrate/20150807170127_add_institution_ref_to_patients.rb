class AddInstitutionRefToPatients < ActiveRecord::Migration
  def change
    add_reference :patients, :institution, index: true, foreign_key: true
  end
end
