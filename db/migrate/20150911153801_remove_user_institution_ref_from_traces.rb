class RemoveUserInstitutionRefFromTraces < ActiveRecord::Migration
  def change
    remove_reference :traces, :user_institution, index: true, foreign_key: true
  end
end
