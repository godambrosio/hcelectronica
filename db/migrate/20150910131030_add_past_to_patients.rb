class AddPastToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :past, :text
  end
end
