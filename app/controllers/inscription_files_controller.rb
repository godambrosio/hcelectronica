class InscriptionFilesController < ApplicationController
  def new
    @inscription = Inscription.find(params[:inscription_id])
    @inscriptionFile = InscriptionFile.new
    @user_institution = @inscription.user_institution
    @patient = @inscription.patient
  end

  def create
    @inscription = Inscription.find(params[:inscription_id])
    @inscriptionFile = InscriptionFile.new(inscriptionFile_params)
    @inscriptionFile.inscription = @inscription
    @patient = @inscription.patient
    @user_institution = @inscription.user_institution
    respond_to do |format|
      if @inscriptionFile.save
        format.html { redirect_to user_institution_patient_path(@user_institution,@patient), notice: 'File was successfully created.' }
        format.json { render :show, status: :created, location: @inscriptionFile }
      else
        format.html { render :new }
        format.json { render json: @inscriptionFile.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @inscription = Inscription.find(params[:inscription_id])
    @inscriptionFile = InscriptionFile.find(params[:id])
    @patient = @inscription.patient
    @user_institution = @inscription.user_institution
  end

  def update
    @inscriptionFile = InscriptionFile.find(params[:id])
    @inscription = Inscription.find(params[:inscription_id])
    @patient = @inscription.patient
    @user_institution = @inscription.user_institution
    respond_to do |format|
      if @inscriptionFile.update(inscriptionFile_params) then
        format.html {redirect_to user_institution_patient_path(@user_institution,@patient), notice: 'File was successfully updated'}
      end
    end
  end

def destroy
  @inscription = Inscription.find(params[:inscription_id])
  @inscriptionFile = InscriptionFile.find(params[:id])
  @patient = @inscription.patient
  @user_institution = @inscription.user_institution
  respond_to do |format|
    if @inscriptionFile.destroy then
      format.html {redirect_to user_institution_patient_path(@user_institution, @patient), notice: 'File deleted'}
    end
  end
end

  private

  def inscriptionFile_params
    params.require(:inscription_file).permit(:name, :description, :link)
  end
end
