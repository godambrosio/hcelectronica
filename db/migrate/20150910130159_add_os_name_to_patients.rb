class AddOsNameToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :os_name, :string
  end
end
