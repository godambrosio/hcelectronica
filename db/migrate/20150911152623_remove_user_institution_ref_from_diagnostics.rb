class RemoveUserInstitutionRefFromDiagnostics < ActiveRecord::Migration
  def change
    remove_reference :diagnostics, :user_institution, index: true, foreign_key: true
  end
end
