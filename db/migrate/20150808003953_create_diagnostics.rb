class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.text :description
      t.text :medicament
      t.date :visit

      t.timestamps null: false
    end
  end
end
