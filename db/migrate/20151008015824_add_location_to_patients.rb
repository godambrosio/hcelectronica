class AddLocationToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :location, :string
  end
end
