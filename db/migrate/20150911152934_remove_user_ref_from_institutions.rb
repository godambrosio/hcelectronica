class RemoveUserRefFromInstitutions < ActiveRecord::Migration
  def change
    remove_reference :institutions, :user, index: true, foreign_key: true
  end
end
