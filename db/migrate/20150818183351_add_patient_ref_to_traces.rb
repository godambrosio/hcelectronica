class AddPatientRefToTraces < ActiveRecord::Migration
  def change
    add_reference :traces, :patient, index: true, foreign_key: true
  end
end
