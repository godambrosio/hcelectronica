class AddBirthdateToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :birthdate, :date
  end
end
