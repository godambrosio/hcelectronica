class AddUserInstitutionRefToTraces < ActiveRecord::Migration
  def change
    add_reference :traces, :user_institution, index: true, foreign_key: true
  end
end
