json.array!(@traces) do |trace|
  json.extract! trace, :id, :description
  json.url trace_url(trace, format: :json)
end
