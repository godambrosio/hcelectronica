class AddPastFamilyToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :past_family, :text
  end
end
