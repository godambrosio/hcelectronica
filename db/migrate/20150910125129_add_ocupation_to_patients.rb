class AddOcupationToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :occupation, :string
  end
end
