class AddInscriptionRefToDiagnostics < ActiveRecord::Migration
  def change
    add_reference :diagnostics, :inscription, index: true, foreign_key: true
  end
end
