class Patient < ActiveRecord::Base
  has_many :inscriptions
  has_many :user_institutions, through: :inscriptions

  validates :identification_document, uniqueness: true
  
  def age
    if (birthdate != nil) then
      now = Time.now.utc
      now.year - birthdate.year - (birthdate.to_time.change(:year => now.year) > now ? 1 : 0)
    else
      nil
    end
  end
end
