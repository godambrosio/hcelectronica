class Inscription < ActiveRecord::Base
  belongs_to :user_institution
  belongs_to :patient

  has_many :diagnostics
  has_many :traces
  has_many :inscription_files
end
