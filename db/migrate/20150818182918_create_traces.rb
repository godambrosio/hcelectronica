class CreateTraces < ActiveRecord::Migration
  def change
    create_table :traces do |t|
      t.text :description

      t.timestamps null: false
    end
  end
end
