class AddPersonalPhoneToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :personal_phone, :string
  end
end
