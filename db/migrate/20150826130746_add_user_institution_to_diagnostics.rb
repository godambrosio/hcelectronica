class AddUserInstitutionToDiagnostics < ActiveRecord::Migration
  def change
    add_reference :diagnostics, :user_institution, index: true, foreign_key: true
  end
end
