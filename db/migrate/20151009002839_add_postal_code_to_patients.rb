class AddPostalCodeToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :postal_code, :integer
  end
end
