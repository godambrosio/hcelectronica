class CreateInscriptionFiles < ActiveRecord::Migration
  def change
    create_table :inscription_files do |t|
      t.string :name
      t.text :description
      t.text :link
      t.references :inscription, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
