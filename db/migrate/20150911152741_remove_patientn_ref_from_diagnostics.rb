class RemovePatientnRefFromDiagnostics < ActiveRecord::Migration
  def change
    remove_reference :diagnostics, :patient, index: true, foreign_key: true
  end
end
