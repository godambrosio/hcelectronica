class AddOthersToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :others, :text
  end
end
