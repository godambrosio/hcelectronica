class InscriptionsController < ApplicationController
  def new
    @user_institution = UserInstitution.find(params[:user_institution_id])
    @patients = Patient.all
  end

  def create
    @user_institution = UserInstitution.find(params[:user_institution_id])
    if (params[:user_institution]) then
      if (@user_institution.patient_ids.any?) then
        params[:user_institution][:patient_ids].each do |patient |
          @user_institution.patient_ids = @user_institution.patient_ids << patient
        end
      else
        @user_institution.patient_ids =  params[:user_institution][:patient_ids]
      end
      if @user_institution.save then
        respond_to do |format|
          format.html {redirect_to user_institution_patients_path(@user_institution), notice: "Succesfully asociated"}
        end
      end
    else
      redirect_to new_user_institution_inscription_path(@user_institution), notice: "Nothing selected to associate"
    end
  end
end
