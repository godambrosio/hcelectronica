class RemoveInstitutionRefFromPatients < ActiveRecord::Migration
  def change
    remove_reference :patients, :institution, index: true, foreign_key: true
  end
end
