class AddIdentificationDocumentToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :identification_document, :string
  end
end
